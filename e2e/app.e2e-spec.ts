import { SantiPortfolioPage } from './app.po';

describe('santi-portfolio App', function() {
  let page: SantiPortfolioPage;

  beforeEach(() => {
    page = new SantiPortfolioPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
